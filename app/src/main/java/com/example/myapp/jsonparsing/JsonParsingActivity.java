package com.example.myapp.jsonparsing;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JsonParsingActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private String TAG = JsonParsingActivity.class.getSimpleName();
    public String url = "https://api.androidhive.info/contacts/";
    //Api to check after post call http://hmkcode.appspot.com/post-json/index.html
    List<Contact> contactList;
    ImageView imageView;
    Button getImage,sendData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_parsing);

        imageView = findViewById(R.id.imageView);
        getImage = findViewById(R.id.getImage);
        sendData = findViewById(R.id.send_data);
        getImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*MyImageTask m = new MyImageTask();
                m.execute("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");*/
                Picasso.with(JsonParsingActivity.this).load("https://homepages.cae.wisc.edu/~ece533/images/airplane.png")
                        .into(imageView);
            }
        });

        sendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyTaskSend send = new MyTaskSend();
                send.execute();
            }
        });

        contactList = new ArrayList<>();
        GetContacts getContacts = new GetContacts();
        getContacts.execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(JsonParsingActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeGetServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        String id = c.getString("id");
                        String name = c.getString("name");
                        String email = c.getString("email");
                        String address = c.getString("address");
                        String gender = c.getString("gender");

                        // Phone node is JSON Object
                        JSONObject json_phone = c.getJSONObject("phone");
                        String mobile = json_phone.getString("mobile");
                        String home = json_phone.getString("home");
                        String office = json_phone.getString("office");

                        // tmp hash map for single contact
                        //HashMap<String, String> contact = new HashMap<>();

                        Contact contact = new Contact();
                        contact.setId(id);
                        contact.setName(name);
                        contact.setEmail(email);
                        contact.setAddress(address);
                        contact.setGender(gender);

                        Phone phone = new Phone();
                        phone.setMobile(mobile);
                        phone.setHome(home);
                        phone.setOffice(office);

                        contact.setPhone(phone);

                        // adding each child node to HashMap key => value
                        /*contact.put("id", id);
                        contact.put("name", name);
                        contact.put("email", email);
                        contact.put("mobile", mobile);*/

                        // adding contact to contact list
                        //contactList.add(contact);
                        contactList.add(contact);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            for (Contact cntct:contactList) {
                System.out.println("Values: "+cntct.toString());
            }
        }

    }
    /**
     * Async task class to get image by making HTTP call
     */
    private  class MyImageTask extends AsyncTask<String,Void, Bitmap>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(JsonParsingActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL myurl = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
                InputStream is = con.getInputStream();
                Bitmap image = BitmapFactory.decodeStream(is);
                //imageView.setImageBitmap(image);
                return image;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            imageView.setImageBitmap(bitmap);
            super.onPostExecute(bitmap);
        }
    }
    /**
     * Async task class to send json by making HTTP POST call
     */
    private class MyTaskSend extends AsyncTask<String,Void,Integer>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(JsonParsingActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                //let us prepare json data to send to server
                JSONObject jdata= new JSONObject();
                jdata.accumulate("name","b_31");
                jdata.accumulate("country","India");
                jdata.accumulate("twitter","b31@twitter.com");
                HttpHandler httpHandler = new HttpHandler();
                int res = httpHandler.makePostServiceCall("http://hmkcode.appspot.com/jsonservlet",jdata);
                return res;
            }catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer code) {
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            if (code == HttpURLConnection.HTTP_ACCEPTED||code==HttpURLConnection.HTTP_OK){
                Toast.makeText(JsonParsingActivity.this, "Success", Toast.LENGTH_SHORT).show();
            } else{
                //tv.setText("Failure");
                Toast.makeText(JsonParsingActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(code);
        }
    }
}
package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myapp.adapter.MyRecyclerViewAdapter;
import com.example.myapp.model.User;
import com.example.myapp.model.UserCache;

import java.util.List;

public class ThirdActivity extends AppCompatActivity {
    RecyclerView  recyclerView;
    List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        recyclerView = findViewById(R.id.recycler_view);
        //recyclerView.setLayoutManager(new LinearLayoutManager(ThirdActivity.this));
        recyclerView.setLayoutManager(new GridLayoutManager(ThirdActivity.this,3));

        //initialized user list
        /*userList = new ArrayList<>();
        userList.add(new User(1,"Xyz","134566789","India"));
        userList.add(new User(2,"Mno","134566789","Iraq"));
        userList.add(new User(3,"Pqr","134566789","USA"));
        userList.add(new User(4,"Abc","134566789","Nepal"));
        userList.add(new User(5,"Def","134566789","China"));
        userList.add(new User(6,"Ghi","134566789","Iran"));*/

        userList = UserCache.getInstance().getUserList();

        MyRecyclerViewAdapter viewAdapter = new MyRecyclerViewAdapter(userList,ThirdActivity.this);
        recyclerView.setAdapter(viewAdapter);
        System.out.println(TAG+"Oncreate Called");
    }


    //activity lifecycle methods
    String TAG = "ThirdActivity";
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println(TAG+" OnStart Method Called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(TAG+" OnResume Method Called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println(TAG+" OnRestart Method Called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println(TAG+" OnPause Method Called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println(TAG+" OnStop Method Called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println(TAG+" OnDestroy Method Called");
    }
}

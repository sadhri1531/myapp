package com.example.myapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.fragments.LoginFragment;
import com.example.myapp.fragments.SignUpFragment;
import com.example.myapp.listners.OnFragmentInteraction;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapp.utility.*;

public class FragExampleActivity extends AppCompatActivity implements OnFragmentInteraction {

    LinearLayout layout;
    Button for_login,for_signup;
    LoginFragment loginFragment;
    SignUpFragment signUpFragment;
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    UserDatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag_example);

        LanguageManager.setLocale(FragExampleActivity.this,"hi");

        layout = findViewById(R.id.main_layout);
//        for_login = findViewById(R.id.for_login);
//        for_signup = findViewById(R.id.for_signup);

        loginFragment = new LoginFragment();
        signUpFragment = new SignUpFragment();

        onFragmentListner(Constants.LOGIN,null);

        /*for_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(loginFragment);
            }
        });
        for_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(signUpFragment);
            }
        });*/

        System.out.println(TAG+" Oncreate Called");

    }
    //Function for adding fragment
    public void addFragment(Fragment fragment,String tag){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.main_layout,fragment,null);
        transaction.commit();
        //for adding fragmnet in stack
        if (manager.getBackStackEntryCount()>1)
            manager.popBackStackImmediate();
        transaction.addToBackStack(tag);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main_layout, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
    private void replaceFragment(Fragment fragment,String tag){
        String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    //function for using fragment dynamically using interface
    @Override
    public void onFragmentListner(int id, Object o) {
        switch (id){
            case Constants.LOGIN:
                //addFragment(loginFragment,BACK_STACK_ROOT_TAG);
                replaceFragment(loginFragment);
                break;
            case Constants.SIGNUP:
                //addFragment(signUpFragment,null);
                replaceFragment(signUpFragment);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }
    }
    //0->Activity 1->Login 2->Signup
    @Override
    public void onBackPressed() {
        //preventing the last fragment to move out
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    //activity lifecycle methods
    String TAG = "FragExampleActivity";
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println(TAG+" OnStart Method Called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(TAG+" OnResume Method Called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println(TAG+" OnRestart Method Called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println(TAG+" OnPause Method Called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println(TAG+" OnStop Method Called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println(TAG+" OnDestroy Method Called");
    }
}

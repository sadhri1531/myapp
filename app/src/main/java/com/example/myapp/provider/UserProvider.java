package com.example.myapp.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.model.User;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class UserProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.example.myapp.provider.UserProvider";
    static final String URL = "content://"+PROVIDER_NAME+"/users";
    //exmample:- content://com.example.myapp.provider.UserProvider/users
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final String _ID = UserDatabaseHandler.KEY_ID;
    static final String NAME = UserDatabaseHandler.KEY_NAME;
    static final String EMAIL = UserDatabaseHandler.KEY_EMAIL;

    private static HashMap<String, String> USER_PROJECTION_MAP;

    static final int USERS = 1;
    static final int USER_ID = 2;

    SQLiteDatabase db;
    UserDatabaseHandler databaseHandler;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "users", USERS);
        uriMatcher.addURI(PROVIDER_NAME, "students/#", USER_ID);
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        databaseHandler = new UserDatabaseHandler(context);
        db = databaseHandler.getWritableDatabase();
        return db != null? true:false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(UserDatabaseHandler.TABLE_NAME);
        db = databaseHandler.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case USERS:
                qb.setProjectionMap(USER_PROJECTION_MAP);
                break;
            case USER_ID:
                qb.appendWhere( _ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
        }

        if (sortOrder == null || sortOrder == ""){
            /**
             * By default sort on student names
             */
            sortOrder = NAME;
        }


        Cursor c = qb.query(db,	projection,	selection,
                selectionArgs,null, null, sortOrder);
        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            /**
             * Get all users records
             */
            case USERS:
                return "vnd.android.cursor.dir/vnd.com.example.myapp.provider.UserProvider.users";
            /**
             * Get a particular user
             */
            case USER_ID:
                return "vnd.android.cursor.item/vnd.com.example.myapp.provider.UserProvider.users";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        /**
         * Add a new student record
         */
         //long rowID = db.insert(UserDatabaseHandler.TABLE_NAME, "", values);
        User user = new User();
        user.setName(values.getAsString(NAME));
        user.setEmailId(values.getAsString(EMAIL));
        long rowID = databaseHandler.addUser(user);
        /**
         * If record is added successfully
         */
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }

        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)){
            case USERS:
                count = db.delete(UserDatabaseHandler.TABLE_NAME, selection, selectionArgs);
                break;

            case USER_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete( UserDatabaseHandler.TABLE_NAME, _ID +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
            int count = 0;
            switch (uriMatcher.match(uri)) {
                case USERS:
                    count = db.update(UserDatabaseHandler.TABLE_NAME, values, selection, selectionArgs);
                    break;

                case USER_ID:
                    count = db.update(UserDatabaseHandler.TABLE_NAME, values,
                            _ID + " = " + uri.getPathSegments().get(1) +
                                    (!TextUtils.isEmpty(selection) ? " AND (" +selection + ')' : ""), selectionArgs);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri );
            }

            getContext().getContentResolver().notifyChange(uri, null);
            return count;
    }
}

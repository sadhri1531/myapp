package com.example.myapp.provider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapp.R;

public class ProviderActivity extends AppCompatActivity {

    EditText id, name, email;
    UserProvider userProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);
        id  = findViewById(R.id.editText1);
        name  = findViewById(R.id.editText2);
        email  = findViewById(R.id.editText3);
        userProvider = new UserProvider();
    }

    public void onClickAddName(View view) {
        ContentValues values = new ContentValues();
        values.put(UserProvider.NAME, name.getText().toString());

        values.put(UserProvider.EMAIL, email.getText().toString());

        Uri uri = getContentResolver().insert(UserProvider.CONTENT_URI, values);
        //Uri uri = userProvider.insert(UserProvider.CONTENT_URI, values);

        Toast.makeText(getBaseContext(),
                uri.toString(), Toast.LENGTH_LONG).show();
    }

    public void onClickRetrieveStudents(View view) {
        String URL = "content://com.example.myapp.provider.UserProvider/users";

        Uri user_uri = Uri.parse(URL);
        Cursor c = managedQuery(user_uri, null, null, null, "name");

        if (c.moveToFirst()) {
            do{
                Toast.makeText(this,
                        c.getString(c.getColumnIndex(UserProvider._ID)) +
                                ", " +  c.getString(c.getColumnIndex( UserProvider.NAME)) +
                                ", " + c.getString(c.getColumnIndex( UserProvider.EMAIL)),
                        Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
    }
}
package com.example.myapp.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.myapp.model.User;

import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class PreferncesCredentials {


    //save data in shared preferences
    public static void savePrefData(String username, String password,boolean isLoggedIn,Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.MY_FILE_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username",username);
        editor.putString("password", password);
        editor.putBoolean("loggedIn",isLoggedIn);
        editor.commit();
    }


    public static User readPrefs(Context context){
        User user = new User();
        SharedPreferences preferences = context.getSharedPreferences(Constants.MY_FILE_PREFS, MODE_PRIVATE);
        user.setEmailId(preferences.getString("username",null));
        user.setPassword(preferences.getString("password",null));
        System.out.println("Saved Credentials: "+user.getEmailId()+ " "+ user.getPassword());
        user.setLoggedIn(preferences.getBoolean("loggedIn",false));
        return user;
    }

    public static Object checkValueType(SharedPreferences sharedPreferences, Preference preference){
        Object obj = null;
        Map<String,?> keys = sharedPreferences.getAll();
        if (keys.get(preference.getKey()).getClass().equals(String.class)) {
            Log.d("data type", "String");
            return PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getString(preference.getKey(), "");
        }else if (keys.get(preference.getKey()).getClass().equals(Integer.class)) {
            Log.d("data type", "Integer");
            return PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getInt(preference.getKey(), 0);
        }else if (keys.get(preference.getKey()).getClass().equals(Boolean.class)) {
            Log.d("data type", "boolean");
            return PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getBoolean(preference.getKey(), false);
        }
        /*
        Map<String,?> keys = sharedPreferences.getAll();
        for(Map.Entry<String,?> entry : keys.entrySet())
        {
          Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
          Log.d("data type", entry.getValue().getClass().toString());

          if ( entry.getValue().getClass().equals(String.class))
            Log.d("data type", "String");
          else if ( entry.getValue().getClass().equals(Integer.class))
            Log.d("data type", "Integer");
          else if ( entry.getValue().getClass().equals(Boolean.class))
            Log.d("data type", "boolean");

        }*/
        return obj;
    }
}

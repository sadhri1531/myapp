package com.example.myapp.utility;

public class Constants {
    public static final int LOGIN = 1;
    public static final int SIGNUP = 2;
    public static final String MY_FILE_PREFS = "my_shared_prefs";
}

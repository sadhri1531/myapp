package com.example.myapp.dialogbox;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.myapp.MainActivity;
import com.example.myapp.R;
import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.model.User;

import java.util.Calendar;

import androidx.appcompat.app.AlertDialog;

public class DialogBox {

    UserDatabaseHandler databaseHandler;

    private int mYear, mMonth, mDay, mHour, mMinute;
    //date picker
    public void openDateDialog(View view, final Context context) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Toast.makeText(context, dayOfMonth + "-" + (monthOfYear + 1) + "-" + year, Toast.LENGTH_SHORT).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    //time picker
    public void openTimeDialog(View view, final Context context) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        Toast.makeText(context, hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    public void alertPopup(final Context context){
        // Dialog builder object for building it
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Alert!").setMessage("Do you want to close application?");
        builder.setCancelable(false);
        //positive button
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //context.finish();
            }
        });
        //negative button
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void alertPopup(final User user,Context context){
        databaseHandler = new UserDatabaseHandler(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Alert!").setMessage(user.getName()+"\n"+user.getId()+"\n"+user.getPhone()+"\n"+
                user.getAddress()+"\n"+user.getEmailId());
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.deleteUser(user);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.updateUser(user);
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    //custom dialog
    public void customDialog(final User user, Context context){
        databaseHandler = new UserDatabaseHandler(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.custom_dialog,null);
        builder.setView(view);

        final EditText name = view.findViewById(R.id.name);
        final EditText pwd = view.findViewById(R.id.password);
        final EditText phone = view.findViewById(R.id.emailId);
        final EditText email = view.findViewById(R.id.phone);
        final EditText address = view.findViewById(R.id.address);
        name.setText(user.getName());
        pwd.setText(user.getPassword());
        phone.setText(user.getPhone());
        email.setText(user.getEmailId());
        address.setText(user.getAddress());

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                user.setName(name.getText().toString());
                user.setPassword(pwd.getText().toString());
                user.setPhone(phone.getText().toString());
                user.setEmailId(email.getText().toString());
                user.setAddress(address.getText().toString());
                databaseHandler.updateUser(user);
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.deleteUser(user);
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }
}

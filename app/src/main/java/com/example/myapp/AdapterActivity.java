package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapp.adapter.MyRecyclerViewAdapter;
import com.example.myapp.model.User;

import java.util.ArrayList;
import java.util.List;
//Implementation of recycler view with smart data transition
public class AdapterActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    List<User> data;
    MyRecyclerViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // data to populate the RecyclerView with
        data = new ArrayList<>();
        data.add(addUsers(1,"Xyz","134566789","India"));
        data.add(addUsers(2,"Mno","134566789","Iraq"));
        data.add(addUsers(3,"Pqr","134566789","USA"));
        data.add(addUsers(4,"Abc","134566789","Nepal"));
        data.add(addUsers(5,"Def","134566789","China"));
        data.add(addUsers(6,"Ghi","134566789","Iran"));
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        adapter = new MyRecyclerViewAdapter(data,this);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    public User addUsers(int i, String name, String phone, String address){
        //User user = new User(i,name,phone,address);
        User user = new User();
        user.setId(i);
        user.setName(name);
        user.setPhone(phone);
        user.setAddress(address);
        return user;
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + adapter.getItem(position).getName() + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

    public void insertSingleItem(View view) {
        User item = addUsers(7,"Mno","134566789","Iraq");
        int insertIndex = 2;
        data.add(insertIndex, item);
        adapter.notifyItemInserted(insertIndex);
    }

    public void insertMultipleItems(View view) {
        ArrayList<User> items = new ArrayList<>();
        items.add(addUsers(8,"User8","134566789","Iraq"));
        items.add(addUsers(9,"User9","134566789","Israel"));
        items.add(addUsers(10,"User10","134566789","Vietnam"));
        int insertIndex = 6;
        data.addAll(insertIndex, items);
        adapter.notifyItemRangeInserted(insertIndex, items.size());
    }

    public void removeSingleItem(View view) {
        int removeIndex = 2;
        data.remove(removeIndex);
        adapter.notifyItemRemoved(removeIndex);
    }

    public void removeMultipleItems(View view) {
        int startIndex = 2; // inclusive
        int endIndex = 4;   // exclusive
        int count = endIndex - startIndex; // 2 items will be removed
        data.subList(startIndex, endIndex).clear();
        adapter.notifyItemRangeRemoved(startIndex, count);
    }

    public void removeAllItems(View view) {
        data.clear();
        adapter.notifyDataSetChanged();
    }

    public void replaceOldListWithNewList(View view) {
        // clear old list
        data.clear();
        // add new list
        ArrayList<User> newList = new ArrayList<>();
        newList.add(addUsers(11,"User11","134566789","Iraq"));
        newList.add(addUsers(12,"User12","134566789","Israel"));
        newList.add(addUsers(13,"User13","134566789","Vietnam"));
        data.addAll(newList);
        // notify adapter
        adapter.notifyDataSetChanged();
    }

    public void updateSingleItem(View view) {
        User newValue = addUsers(3,"Renamed Mno","134566789","Iraq");
        int updateIndex = 3;
        data.set(updateIndex, newValue);
        adapter.notifyItemChanged(updateIndex);
    }

    public void moveSingleItem(View view) {
        int fromPosition = 3;
        int toPosition = 1;
        // update data array
        User item = data.get(fromPosition);
        data.remove(fromPosition);
        data.add(toPosition, item);
        // notify adapter
        adapter.notifyItemMoved(fromPosition, toPosition);
    }

}
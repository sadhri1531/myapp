package com.example.myapp.model;

import android.os.Build;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.RequiresApi;

public class User implements Serializable {

    private int id;
    private String name;
    private String phone;
    private String address;
    private String password;
    private String emailId;
    private Boolean isLoggedIn;

    public User(int id, String name, String phone, String address, String password, String emailId) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.password = password;
        this.emailId = emailId;
    }


    public User(int id, String name, String phone, String address) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public User(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Boolean getLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getName().equals(user.getName()) &&
                getPhone().equals(user.getPhone()) &&
                getAddress().equals(user.getAddress()) &&
                getPassword().equals(user.getPassword()) &&
                getEmailId().equals(user.getEmailId()) &&
                isLoggedIn.equals(user.isLoggedIn);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPhone(), getAddress(), getPassword(), getEmailId(), isLoggedIn);
    }
}

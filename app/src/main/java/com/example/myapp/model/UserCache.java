package com.example.myapp.model;

import java.util.List;

public class UserCache {

    private User user;
    private List<User> userList;
    private static UserCache userCache;

    private UserCache() {}

    public static UserCache getInstance(){
        if (null == userCache) {
            userCache = new UserCache();
        }
        return userCache;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}

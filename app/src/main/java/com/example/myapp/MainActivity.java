package com.example.myapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.myapp.adapter.MyAdapter;
import com.example.myapp.animation.AnimationSample;
import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.jsonparsing.JsonParsingActivity;
import com.example.myapp.listners.OnFragmentInteraction;
import com.example.myapp.map_loc.MapsActivity;
import com.example.myapp.model.User;
import com.example.myapp.model.UserCache;
import com.example.myapp.notification.MyNotification;
import com.example.myapp.provider.ProviderActivity;
import com.example.myapp.service.MyService;
import com.example.myapp.service.MyServiceActivity;
import com.example.myapp.settings_preference.MyActivity;
import com.example.myapp.tabview.TabViewSampleActivity;
import com.example.myapp.utility.Constants;
import com.example.myapp.utility.LanguageManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnFragmentInteraction {

    Button btn1,btn2,btn3;

    String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    int PERMS_REQUEST_CODE=200;

    List<User>  userList;

    ListView listView;
    private MyAdapter myAdapter;

    UserDatabaseHandler databaseHandler;

    Intent serviceIntent;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        LanguageManager.setLocale(context,"hi");
        for (int i = 0;i < perms.length; i++){
            if (!checkPermissions(perms[i])){
                requestpermissions(perms);
            }
        }

        MyNotification.createNotificationChannel(MainActivity.this);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.my_image);
        MyNotification.crateBigPictureStyleNotification(context,"My Title","My Big Image", bitmap);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            MyNotification.displayNotification(context,111);
        }
        //initialized user list
        userList = new ArrayList<>();
        /*userList.add(addUsers(1,"Xyz","134566789","India"));
        userList.add(addUsers(2,"Mno","134566789","Iraq"));
        userList.add(addUsers(3,"Pqr","134566789","USA"));
        userList.add(addUsers(4,"Abc","134566789","Nepal"));
        userList.add(addUsers(5,"Def","134566789","China"));
        userList.add(addUsers(6,"Ghi","134566789","Iran"));*/
        //database
        databaseHandler = new UserDatabaseHandler(MainActivity.this);
        userList = databaseHandler.getAllUser();
        //Chaching variable to use in multiple activties throughout the application
        UserCache userCache = UserCache.getInstance();
        userCache.setUserList(userList);

        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        listView = findViewById(R.id.listView);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //implicitIntent();
                explicitIntent();
                //alertPopup();
                Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,FragExampleActivity.class);
                startActivity(i);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, TabViewSampleActivity.class);
                startActivity(i);
            }
        });

        myAdapter = new MyAdapter(MainActivity.this,userList);
        listView.setAdapter(myAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, userList.get(position).getName(), Toast.LENGTH_SHORT).show();
                //alertPopup(userList.get(position));
                customDialog(userList.get(position));
                userList = databaseHandler.getAllUser();
                //myAdapter = new MyAdapter(MainActivity.this,userList);
                //listView.setAdapter(myAdapter);
                if (myAdapter!=null)
                    myAdapter.updateDataList(userList);
            }
        });

        /*LoginFragment loginFragment = new LoginFragment();
        addFragment(loginFragment);*/
        System.out.println(TAG+" Oncreate Called");
    }
    //function to add user data using setter method
    public User addUsers(int i,String name,String phone,String address){
        //User user = new User(i,name,phone,address);
        User user = new User();
        user.setId(i);
        user.setName(name);
        user.setPhone(phone);
        user.setAddress(address);
        return user;
    }

    public void implicitIntent(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.vogella.com/tutorials/AndroidIntent/article.html"));
        startActivity(intent);
    }

    public void explicitIntent(){
        Intent intent = new Intent(MainActivity.this,SecondActivity.class);
        intent.putExtra("name","Santosh");
        intent.putExtra("city","Bengaluru");
        //startActivity(intent);
        startActivityForResult(intent,201);
    }

    public boolean checkPermissions(){
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkPermissions(String perms_name){
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), perms_name);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void requestpermissions(){
        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMS_REQUEST_CODE);
    }

    public void requestpermissions(String[] perms_array){
        ActivityCompat.requestPermissions(MainActivity.this, perms_array ,PERMS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 201 && resultCode == RESULT_OK){
            User user = (User) data.getSerializableExtra("user_obj");
            userList.add(user);
            //myAdapter.notifyDataSetChanged();
            myAdapter.updateDataList(userList);
        }
    }

    @Override
    public void onFragmentListner(int id, Object o) {
        if (id == Constants.LOGIN){

        }
    }

    //activity lifecycle methods
    String TAG = "MainActivity";
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println(TAG+" OnStart Method Called");
        //userList = databaseHandler.getAllUser();
        userList.addAll(databaseHandler.getAllUser());
        //myAdapter = new MyAdapter(MainActivity.this,userList);
        //listView.setAdapter(myAdapter);
        if (myAdapter!=null)
            myAdapter.updateDataList(userList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(TAG+" OnResume Method Called");

        serviceIntent = new Intent(MainActivity.this, MyService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent);
        }else {
            startService(serviceIntent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println(TAG+" OnRestart Method Called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println(TAG+" OnPause Method Called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println(TAG+" OnStop Method Called");
        stopService(serviceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println(TAG+" OnDestroy Method Called");
    }

    public void alertPopup(){
        // Dialog builder object for building it
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Alert!").setMessage("Do you want to close application?");
        builder.setCancelable(false);
        //positive button
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        //negative button
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void alertPopup(final User user){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Alert!").setMessage(user.getName()+"\n"+user.getId()+"\n"+user.getPhone()+"\n"+
                user.getAddress()+"\n"+user.getEmailId());
        builder.setCancelable(false);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.deleteUser(user);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.updateUser(user);
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    //custom dialog
    public void customDialog(final User user){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.custom_dialog,null);
        builder.setView(view);

        final EditText name = view.findViewById(R.id.name);
        final EditText pwd = view.findViewById(R.id.password);
        final EditText phone = view.findViewById(R.id.emailId);
        final EditText email = view.findViewById(R.id.phone);
        final EditText address = view.findViewById(R.id.address);
        name.setText(user.getName());
        pwd.setText(user.getPassword());
        phone.setText(user.getPhone());
        email.setText(user.getEmailId());
        address.setText(user.getAddress());

        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                user.setName(name.getText().toString());
                user.setPassword(pwd.getText().toString());
                user.setPhone(phone.getText().toString());
                user.setEmailId(email.getText().toString());
                user.setAddress(address.getText().toString());
                databaseHandler.updateUser(user);
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                databaseHandler.deleteUser(user);
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        alertPopup();
    }

    private int mYear, mMonth, mDay, mHour, mMinute;
    //date picker
    public void openDateDialog(View view) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Toast.makeText(MainActivity.this, dayOfMonth + "-" + (monthOfYear + 1) + "-" + year, Toast.LENGTH_SHORT).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    //time picker
    public void openTimeDialog(View view) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        Toast.makeText(MainActivity.this, hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void goToServiceActivity(View view) {
        Intent intent = new Intent(MainActivity.this, MyServiceActivity.class);
        startActivity(intent);
    }

    public void goToJsonActivity(View view) {
        Intent intent = new Intent(MainActivity.this, JsonParsingActivity.class);
        startActivity(intent);
    }

    public void goToProviderActivity(View view) {
        Intent intent = new Intent(MainActivity.this, ProviderActivity.class);
        startActivity(intent);
    }

    public void goToMapActivity(View view) {
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
        startActivity(intent);
    }

    public void goToMyActivity(View view) {
        Intent intent = new Intent(MainActivity.this, MyActivity.class);
        startActivity(intent);
    }

    public void loadModule(View view) {
        startActivity(new Intent(this, com.kts.apploader.MainActivity.class));
    }

    public void loadAnim(View view) {
        startActivity(new Intent(this, AnimationSample.class));
    }
}

package com.example.myapp.notification;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.myapp.R;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class DirectReplyActivity extends AppCompatActivity {


    private static String channelID = "com.example.myapp.notification";
    private static int notificationId = 101;
    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_reply);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            MyNotification.createNotificationChannel(this,channelID, "DirectReply News", "Example News Channel");
            MyNotification.sendNotification(this,"Direct Reply!","Reply and check the output");
            handleIntent();
        }
    }

    //Method for handling input from notification
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void handleIntent() {

        Intent intent = this.getIntent();

        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

        if (remoteInput != null) {

            //TextView myTextView = findViewById(R.id.textView);
            String inputString = remoteInput.getCharSequence(
                    MyNotification.KEY_TEXT_REPLY).toString();
            ConstraintLayout layout = findViewById(R.id.direct_reply);
            //myTextView.setText(inputString);
            final Snackbar snackbar = Snackbar.make(layout,inputString, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();

            Notification repliedNotification =
                    new Notification.Builder(this, channelID)
                            .setSmallIcon(android.R.drawable.ic_dialog_info)
                            .setContentText("Reply received")
                            .build();

            notificationManager.notify(notificationId, repliedNotification);
        }
    }
}
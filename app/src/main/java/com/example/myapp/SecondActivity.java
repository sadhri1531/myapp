package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapp.model.User;

public class SecondActivity extends AppCompatActivity {

    EditText edit_name,edit_address,edit_phone;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String city = intent.getStringExtra("city");
        Toast.makeText(this, name+"  "+city, Toast.LENGTH_SHORT).show();

        edit_name = findViewById(R.id.txtName);
        edit_address = findViewById(R.id.txtAddr);
        edit_phone = findViewById(R.id.txtPhone);

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent my_intent = new Intent();
                User user = new User();
                user.setName(edit_name.getText().toString());
                user.setAddress(edit_address.getText().toString());
                user.setPhone(edit_phone.getText().toString());
                my_intent.putExtra("user_obj", user);

                setResult(RESULT_OK,my_intent);
                finish();
            }
        });

        System.out.println(TAG+"Oncreate Called");
    }

    //activity lifecycle methods
    String TAG = "SecondActivity";
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println(TAG+" OnStart Method Called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(TAG+" OnResume Method Called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println(TAG+" OnRestart Method Called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println(TAG+" OnPause Method Called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println(TAG+" OnStop Method Called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println(TAG+" OnDestroy Method Called");
    }
}

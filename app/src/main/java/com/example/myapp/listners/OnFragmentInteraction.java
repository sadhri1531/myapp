package com.example.myapp.listners;

/*
* Listner for Loading Fragment
*/
public interface OnFragmentInteraction {
    void onFragmentListner(int id, Object o);
}

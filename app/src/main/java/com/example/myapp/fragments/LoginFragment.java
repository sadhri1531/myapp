package com.example.myapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.FragExampleActivity;
import com.example.myapp.R;
import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.listners.OnFragmentInteraction;
import com.example.myapp.model.User;
import com.example.myapp.utility.Constants;
import com.example.myapp.utility.PreferncesCredentials;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    EditText username,password;
    CheckBox rememberMe;
    Button login;
    TextView signUp;
    Context frag_context;
    //refrence of interface listner
    OnFragmentInteraction onFragmentInteraction;

    String savedUserName,savedUserPwd;

    UserDatabaseHandler databaseHandler;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        username = view.findViewById(R.id.edit_user);
        password = view.findViewById(R.id.edit_pwd);
        login = view.findViewById(R.id.btn_login);
        signUp = view.findViewById(R.id.btn_sign_up);
        rememberMe = view.findViewById(R.id.loggedInCheckBox);

        //check condition if user clicked on remember me for auto login
        User user = PreferncesCredentials.readPrefs(frag_context);
        if (user.getLoggedIn()){
            username.setText(user.getEmailId());
            password.setText(user.getPassword());
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //From Sqlite database
                User dbuser = new User();
                dbuser = databaseHandler.getUserByEmailId(username.getText().toString());
                if (dbuser!=null) {
                    savedUserName = dbuser.getEmailId();
                    savedUserPwd = dbuser.getPassword();
                    if (username.getText().toString().equals(savedUserName) && password.getText().toString().equals(savedUserPwd)) {
                        Toast.makeText(frag_context, "Successful", Toast.LENGTH_SHORT).show();
                        if (rememberMe.isChecked()) {
                            //Toast.makeText(frag_context, "Checkbox: "+rememberMe.isChecked(), Toast.LENGTH_SHORT).show();
                            PreferncesCredentials.savePrefData(username.getText().toString(), password.getText().toString(),
                                    rememberMe.isChecked(), frag_context);
                        }
                    }else
                        Toast.makeText(frag_context, frag_context.getClass().getSimpleName()+" Inavlid username/password", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(frag_context, frag_context.getClass().getSimpleName()+" Data not found/Enter valid user name", Toast.LENGTH_SHORT).show();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(frag_context, "SignUp", Toast.LENGTH_SHORT).show();
                onFragmentInteraction.onFragmentListner(Constants.SIGNUP,null);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.frag_context = context;
        databaseHandler = new UserDatabaseHandler(frag_context);
        if (frag_context instanceof FragExampleActivity)
            onFragmentInteraction = (OnFragmentInteraction) frag_context;
    }


}

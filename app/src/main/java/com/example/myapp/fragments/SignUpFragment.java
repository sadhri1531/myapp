package com.example.myapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapp.FragExampleActivity;
import com.example.myapp.R;
import com.example.myapp.database.UserDatabaseHandler;
import com.example.myapp.listners.OnFragmentInteraction;
import com.example.myapp.model.User;
import com.example.myapp.utility.Constants;
import com.example.myapp.utility.PreferncesCredentials;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    EditText txtName,txtEmail,txtAddress,txtPhone,txtPassword;
    Button submit;
    OnFragmentInteraction onFragmentInteraction;
    private Context frag_context;

    UserDatabaseHandler databaseHandler;

    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        txtName = view.findViewById(R.id.txtName);
        txtAddress = view.findViewById(R.id.txtAddr);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtPhone = view.findViewById(R.id.txtPhone);
        txtPassword = view.findViewById(R.id.txtPassword);

        submit = view.findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Shred preferences for saving user and password
                //PreferncesCredentials.savePrefData(txtEmail.getText().toString(),txtPassword.getText().toString(),frag_context);
                User user = new User();
                user.setName(txtName.getText().toString());
                user.setEmailId(txtEmail.getText().toString());
                user.setAddress(txtAddress.getText().toString());
                user.setPhone(txtPhone.getText().toString());
                user.setPassword(txtPassword.getText().toString());
                databaseHandler.addUser(user);
                onFragmentInteraction.onFragmentListner(Constants.LOGIN,null);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.frag_context = context;
        //Initialized db handler object
        databaseHandler = new UserDatabaseHandler(frag_context);
        if (frag_context instanceof FragExampleActivity)
            onFragmentInteraction = (OnFragmentInteraction) frag_context;
    }


}

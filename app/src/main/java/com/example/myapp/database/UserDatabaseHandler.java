package com.example.myapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.myapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rishav on 07-Nov-19.
 */

public class UserDatabaseHandler extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "user_detail";
    public static final String TABLE_NAME = "user";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PH_NO = "phone_number";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_ADDRESS = "address";


    public UserDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DETAILS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PASSWORD + " TEXT," + KEY_EMAIL + " TEXT,"+ KEY_PH_NO + " TEXT,"+ KEY_ADDRESS + " TEXT"+ ")";
        db.execSQL(CREATE_DETAILS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }


    // code to add the new user
    public Long addUser(User details) {
        //Initialize the database as writable
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, details.getName()); // User Name
        values.put(KEY_EMAIL, details.getEmailId()); // User Email
        values.put(KEY_PH_NO, details.getPhone()); // User Phone
        values.put(KEY_PASSWORD, details.getPassword());// User Password
        values.put(KEY_ADDRESS, details.getAddress());// User Address

        // Inserting Row
        Long rowId = db.insert(TABLE_NAME, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection

        return rowId;
    }

    // code to get the single user
    public User getUser(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO, KEY_ADDRESS, KEY_PASSWORD, KEY_EMAIL }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        User details = new User(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),
                cursor.getString(4),cursor.getString(5));
        // return contact
        db.close();
        return details;
    }

    public User getUserByEmailId(String email_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO, KEY_ADDRESS, KEY_PASSWORD, KEY_EMAIL }, KEY_EMAIL + "=?",
                new String[] { email_id }, null, null, null, null);
        if (cursor != null && cursor.moveToNext())
            cursor.moveToFirst();
        else
            return null;

        User details = new User(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),
                cursor.getString(4),cursor.getString(5));
        // return contact
        db.close();
        return details;
    }

    // code to get all contacts in a list view
    public List<User> getAllUser() {
        List<User> detailsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User details = new User();
                details.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID))));
                details.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                details.setPassword(cursor.getString(cursor.getColumnIndex(KEY_PASSWORD)));
                details.setEmailId(cursor.getString(cursor.getColumnIndex(KEY_EMAIL)));
                details.setPhone(cursor.getString(cursor.getColumnIndex(KEY_PH_NO)));
                details.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                // Adding contact to list
                detailsList.add(details);
            } while (cursor.moveToNext());
        }
        db.close();
        // return user list
        return detailsList;
    }

    // code to update the single user
    public int updateUser(User details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, details.getName()); // User Name
        values.put(KEY_EMAIL, details.getEmailId()); // User Email
        values.put(KEY_PH_NO, details.getPhone()); // User Phone
        values.put(KEY_PASSWORD, details.getPassword()); // User Password
        values.put(KEY_ADDRESS, details.getAddress()); // User Address
        //db.update(TABLE_NAME, values, KEY_ID + " = ? "+KEY_PH_NO+ " = ?",
        //        new String[] { String.valueOf(details.getId()),details.getPhone() });
        // updating row
        int res = db.update(TABLE_NAME, values, KEY_ID + " = ? ",
                new String[] { String.valueOf(details.getId()) });
        db.close();
        return res;
    }

    // Deleting single user
    public void deleteUser(User details) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = ?",
                new String[] { String.valueOf(details.getId()) });
        db.close();
    }

}

package com.example.myapp.customloaders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.example.myapp.R;

public class LoaderSampleActivity extends AppCompatActivity {

    private int TASK_ID;
    private Bundle TASK_BUNDLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader_sample);

        getSupportLoaderManager().initLoader(TASK_ID, TASK_BUNDLE, new LoaderManager.LoaderCallbacks<Bitmap>() {
            @Override
            public Loader<Bitmap> onCreateLoader(final int id, final Bundle args) {
                return new ImageLoadingTask(LoaderSampleActivity.this);
            }

            @Override
            public void onLoadFinished(final Loader<Bitmap> loader, final Bitmap result) {
                if (result == null)
                    return;
                //TODO use result
            }

            @Override
            public void onLoaderReset(final Loader<Bitmap> loader) {
            }
        });
    }


    private static class ImageLoadingTask extends AsyncTaskLoaderEx<Bitmap> {

        public ImageLoadingTask (Context context) {
            super(context);
        }

        @Override
        public Bitmap loadInBackground() {
            //TODO load and return bitmap
            return null;
        }
    }
}
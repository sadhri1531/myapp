package com.example.myapp.adapter;

import com.example.myapp.model.User;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

public class UserDiffUtilCallback extends DiffUtil.Callback {
    private final List<User> mOldUserList;
    private final List<User> mNewUserList;

    public UserDiffUtilCallback(List<User> oldUserList, List<User> newUserList) {
        this.mOldUserList = oldUserList;
        this.mNewUserList = newUserList;
    }

    @Override
    public int getOldListSize() {
        return mOldUserList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewUserList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldUserList.get(oldItemPosition).getId() == mNewUserList.get(
                newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final User oldUser = mOldUserList.get(oldItemPosition);
        final User newUser = mNewUserList.get(newItemPosition);

        return oldUser.equals(newUser);
    }

    /*Object returned in getChangePayload() is dispatched from DiffResult using
      notifyItemRangeChanged(position, count, payload), upon which is called Adapter’s
      onBindViewHolder(… List<Object> payloads) method.*/
    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }

}

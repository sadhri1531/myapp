package com.example.myapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapp.R;
import com.example.myapp.model.User;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    List<User> dataList;
    Context context;
    LayoutInflater inflater;

    public MyAdapter(Context cont, List<User> data) {
        this.dataList = data;
        this.context = cont;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_cell,null);
        }
        TextView name = convertView.findViewById(R.id.user_name);
        TextView address = convertView.findViewById(R.id.user_address);
        TextView phone = convertView.findViewById(R.id.user_phone);
        User user = dataList.get(position);
        name.setText(user.getName());
        address.setText(user.getAddress());
        phone.setText(user.getPhone());
        return convertView;
    }

    public void updateDataList(List<User> dataList){
        this.dataList.clear();
        this.dataList = dataList;
        this.notifyDataSetChanged();
    }
}

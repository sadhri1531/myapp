package com.example.myapp.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.MainActivity;
import com.example.myapp.R;
import com.example.myapp.notification.MyNotification;

public class MyServiceActivity extends AppCompatActivity {

    TextView textView;

    //BoundService class Object
    BoundService boundService;
    //boolean variable to keep a check on service bind and unbind event
    boolean isBound = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_service);

        textView = findViewById(R.id.status);
    }

    //dynamic broadcast receiver
    private BroadcastReceiver myreceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string_path = bundle.getString(DownloadService.FILEPATH);
                int resultCode = bundle.getInt(DownloadService.RESULT);
                if (resultCode == RESULT_OK) {
                    Toast.makeText(MyServiceActivity.this,
                            "Download complete. Download URI: " + string_path,
                            Toast.LENGTH_LONG).show();
                    textView.setText("Download done" + string_path);
                    //call notification on successful download
                    MyNotification.createNotificationChannel(MyServiceActivity.this);
                    MyNotification.sendNotification("Done",301,MyServiceActivity.this);
                } else {
                    Toast.makeText(MyServiceActivity.this, "Download failed",
                            Toast.LENGTH_LONG).show();
                    textView.setText("Download failed");
                    MyNotification.createNotificationChannel(MyServiceActivity.this);
                    MyNotification.sendNotification("Failed",301,MyServiceActivity.this);
                }
            }
        }
    };

    public void onClick(View view) {
        Intent intent = new Intent(this, DownloadService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(DownloadService.FILENAME, "file.html");
        //intent.putExtra(DownloadService.FILENAME, "airplane.png");
        intent.putExtra(DownloadService.URL_PATH,"https://www.vogella.com/index.html");
        //intent.putExtra(DownloadService.URL_PATH,"https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
        startService(intent);
        textView.setText("Service started");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //register your broadcast reciever dynamically
        registerReceiver(myreceiver, new IntentFilter(DownloadService.NOTIFICATION));
        //
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyServiceActivity.this,
                        String.valueOf(boundService.randomGenerator()),Toast.LENGTH_SHORT).show();
            }
        };

        Handler handler = new Handler();
        handler.postDelayed(runnable,3000);


    }
    @Override
    protected void onPause() {
        super.onPause();
        //un-register your broadcast reciever dynamically
        unregisterReceiver(myreceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this , BoundService.class);
        startService(intent);
        bindService(intent , boundServiceConnection,BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isBound) {
            unbindService(boundServiceConnection);
            isBound = false;
        }
    }

    private ServiceConnection boundServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundService.MyBinder binderBridge = (BoundService.MyBinder) service ;
            boundService = binderBridge.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            boundService= null;
        }
    };
}
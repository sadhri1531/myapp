package com.example.myapp.tabview;

import com.example.myapp.fragments.Fragment1;
import com.example.myapp.fragments.Fragment2;
import com.example.myapp.fragments.Fragment3;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    String[] titleStrings = {"Tab1","Tab2","Tab3"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new Fragment1();
                break;
            case 1:
                fragment = new Fragment2();
                break;
            case 2:
                fragment = new Fragment3();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return titleStrings.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleStrings[position];
    }
}
